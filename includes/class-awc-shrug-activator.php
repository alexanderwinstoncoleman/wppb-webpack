<?php

/**
 * Fired during plugin activation
 *
 * @link       https://alexcoleman.io
 * @since      1.0.0
 *
 * @package    Awc_Shrug
 * @subpackage Awc_Shrug/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Awc_Shrug
 * @subpackage Awc_Shrug/includes
 * @author     Alex Coleman <alex@alexcoleman.io>
 */
class Awc_Shrug_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
