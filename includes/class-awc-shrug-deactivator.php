<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://alexcoleman.io
 * @since      1.0.0
 *
 * @package    Awc_Shrug
 * @subpackage Awc_Shrug/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Awc_Shrug
 * @subpackage Awc_Shrug/includes
 * @author     Alex Coleman <alex@alexcoleman.io>
 */
class Awc_Shrug_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
