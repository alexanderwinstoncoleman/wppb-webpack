# WebPack + WPPB

A simple implementation of Webpack into the [WordPress Plugin Boilerplate](https://wppb.me/).

## Usage

Well, you don't need the whole thing. This is just an example of how I've assembled this. Sniff around. If you have any questions, feel free to contact me.
