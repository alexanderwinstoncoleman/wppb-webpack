const path = require("path");
const ZipPlugin = require("zip-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const plugSlug = "awc-shrug";
const adminJsSlug = `admin/js/${plugSlug}-admin`;
const publicJsSlug = `public/js/${plugSlug}-public`;

module.exports = {
  entry: {
    // index: path.resolve(__dirname, "src", "index.js"),
    [adminJsSlug]: path.resolve(
      __dirname,
      "src/admin/js",
      "awc-shrug-admin.js"
    ), // the key could be any filename, as that dictates the filename - NOT the entry
    [publicJsSlug]: path.resolve(
      __dirname,
      "src/public/js",
      "awc-shrug-public.js"
    ), // the key could be any filename, as that dictates the filename - NOT the entry
  },
  output: {
    filename: "[name].js",
    // path: __dirname,
    path: path.resolve(__dirname, "./"),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
    ],
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        {
			// FROM and TOO are important here: FROM picks up everything, and TO puts it in the root of the zip file
			from: '**/*',
			to: path.resolve(__dirname, "./"),
			globOptions: {
				ignore: [
				path.resolve(__dirname, "node_modules"),
				path.resolve(__dirname, "dist"),
				path.resolve(__dirname, "src"),
				path.resolve(__dirname, ".git"),
				path.resolve(__dirname, ".gitignore"),
				path.resolve(__dirname, "package.json"),
				path.resolve(__dirname, "package.lock.json"),
				path.resolve(__dirname, "babel.config.json"),
				path.resolve(__dirname, "webpack.config.js"),
				path.resolve(__dirname, `${plugSlug}.zip`),
				path.resolve(__dirname, "DIRECTIONS.txt"),
				],
			},
        },
      ],
    }),
    new ZipPlugin({
		// OPTIONAL: defaults to the Webpack output path (above)
		// can be relative (to Webpack output path) or absolute
		// THE PATH WHERE THE ZIP LANDS
		path: "../",

		// OPTIONAL: defaults to the Webpack output filename (above) or,
		// if not present, the basename of the path
		filename: `${plugSlug}.zip`,

		// OPTIONAL: defaults to 'zip'
		// the file extension to use instead of 'zip'
		//   extension: "ext",

		// OPTIONAL: defaults to the empty string
		// the prefix for the files included in the zip file
		pathPrefix: "",

		// OPTIONAL: defaults to the identity function
		// a function mapping asset paths to new paths
		//   pathMapper: function (assetPath) {
		//     // put all pngs in an `images` subdir
		//     if (assetPath.endsWith(".png"))
		//       return path.join(
		//         path.dirname(assetPath),
		//         "images",
		//         path.basename(assetPath)
		//       );
		//     return assetPath;
		//   },

		// OPTIONAL: defaults to including everything
		// can be a string, a RegExp, or an array of strings and RegExps
		//   include: [/\.js$/],

		// OPTIONAL: defaults to excluding nothing
		// can be a string, a RegExp, or an array of strings and RegExps
		// if a file matches both include and exclude, exclude takes precedence
		exclude: [/\.json$/, /\.zip$/, /\.git$/, /\.gitignore$/, /src$/, /dist$/],
    }),
  ],
};
